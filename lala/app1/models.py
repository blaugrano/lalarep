from django.db import models
from autoslug import AutoSlugField

class Dog(models.Model):
    name = models.CharField(max_length = 30)
    slug = AutoSlugField(populate_from='name', unique=True)
    color = models.CharField(max_length = 30)
    
    def __str__(self):
        return self.name

class Flea(models.Model):
    dog = models.ForeignKey(Dog, on_delete=models.CASCADE)
    
    name = models.CharField(max_length = 30)
    teeth_number = models.IntegerField()
    is_lethal = models.BooleanField()
    phone = models.CharField(max_length = 30)
    arrival_day = models.DateField()
    
    def __str__(self):
        return self.name
    