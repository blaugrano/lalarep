'''
Created on Jan 21, 2018

@author: User01
'''

from django import template
from app1.models import Dog

register = template.Library()

@register.inclusion_tag('dogs.html')
def get_dogs(active_dog=None):
    return {'dogs' : Dog.objects.all(), 'active_dog' : active_dog}
    #return render(request, 'dogs.html', {'dogs': Dog.objects.all()})