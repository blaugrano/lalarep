from django.contrib import admin
from app1.models import Dog, Flea

admin.site.register(Dog)
admin.site.register(Flea)
