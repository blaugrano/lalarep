from django.forms.models import ModelForm
from django.forms.fields import TextInput, DateInput, DateField, CharField, IntegerField, BooleanField
from django.forms.widgets import HiddenInput
from app1.models import Dog, Flea

class DogForm(ModelForm):
    name = CharField(max_length=128, widget=TextInput(attrs={"class" : "form-control",
                                                                   'placeholder': 'Enter dog name here'}),
                                                                   label='Name')
    slug = CharField(widget=HiddenInput, required=False)
    color = CharField(max_length=128, widget=TextInput(attrs={"class" : "form-control",
                                                                   'placeholder': 'Enter dog color here'}),
                                                                   label='Color')
    class Meta:
        model = Dog
        fields = ('name', 'color',)
        
class FleaForm(ModelForm):
    name = CharField(max_length=128, widget=TextInput(attrs={"class" : "form-control",
                                                                   'placeholder': 'Enter flea name here'}),
                                                                   label='Name')
    teeth_number = IntegerField(max_value=99999, widget=TextInput(attrs={"class" : "form-control",
                                                                   'placeholder': 'Enter teeth amount here'}),
                                                                   label='Teeth')
    is_lethal = BooleanField(required=False)
    
     
    phone = CharField(max_length=15, widget=TextInput(attrs={"class" : "form-control bfh-phone",
                                                             "data-format" : "ddd-ddd-dddd"}))
    
    #arrival_day = DateField(input_formats=['%m/%d/%Y'], label="Start Date", widget=DateInput(format='%m/%d/%Y'), help_text="MM/DD/YYYY")
    
    arrival_day = DateField(input_formats=['%m/%d/%Y'], label="Start Date", widget=DateInput({'class': 'datepicker form-control'}))
    

    class Meta:
        model = Flea
        fields = ('name', 'teeth_number', 'is_lethal', 'phone', 'arrival_day',)