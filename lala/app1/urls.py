from django.conf.urls import url
from app1.views import index, dog, add_dog, add_flea, about

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^dog/(?P<dog_slug>[\w\-]+)/$', dog, name='dog'),
    url(r'^add_dog/$', add_dog, name='add_dog'),
    url(r'^dog/(?P<dog_slug>[\w\-]+)/add_flea/$', add_flea, name='add_flea'),
    url(r'^about/$', about, name='about'),
]