from django.shortcuts import render
from app1.models import Dog, Flea
from app1.forms import DogForm, FleaForm
from django.http.response import HttpResponseRedirect


def index(request):
    context = {'dogs' : Dog.objects.order_by('name')}
    return render(request, 'home.html', context)

def about(request):
    return render(request, 'about.html')

def dog(request, dog_slug):
    context_dict = {}
    
    try:
        
        dog = Dog.objects.get(slug=dog_slug)
        context_dict['dog'] = dog
        context_dict['fleas'] = Flea.objects.filter(dog=dog)
        
    except Dog.DoesNotExist:
        pass
    
    return render(request, 'dog_summary.html', context_dict)

def add_dog(request):
    
    if request.method == 'POST':
        form = DogForm(request.POST)
        
        if form.is_valid():
            form.save(commit=True)
            return HttpResponseRedirect('/')
        else:
            print(form.errors)
    else:
        form = DogForm()
    
    return render(request, 'add_dog.html', {'form': form})

def add_flea(request, dog_slug):
    
    dog = Dog.objects.get(slug=dog_slug)

    if request.method == 'POST':
        form = FleaForm(request.POST)
        
        if form.is_valid():
            if dog:
                flea = form.save(commit=False)
                flea.dog = dog
                flea.save()
                return HttpResponseRedirect('/')
                # return HttpResponseRedirect(reverse('dog', kwargs={'dog_slug' : dog_slug}))

        else:
            print(form.errors)
    else:
        form = FleaForm()
    
    return render(request, 'add_flea.html', {'form' : form, 'dog' : dog})


